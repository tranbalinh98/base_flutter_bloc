import 'package:flutter/material.dart';

class AppColors {
  static const primaryColor = Color(0xff2ecc71);
  static const softGreen = Color(0xffF2FFF8);
  static const red = Color(0xffFF2929);
  static const coolRed = Color(0xffFA5051);
  static const warmGrey = Color(0xff949494);
  static const softGrey = Color(0xffF8F8F8);
}
