import 'package:flutter/material.dart';
class AppTextTheme {
  final TextStyle textStyleAppbar = TextStyle(
    fontSize: 18,
    color: Colors.black,
    fontWeight: FontWeight.w700,
  );

  final TextStyle textStyleLabel = TextStyle(
    fontSize: 16,
    color: Colors.black,
    fontWeight: FontWeight.w600,
  );

  final TextStyle textStyleButton = TextStyle(
    fontSize: 16,
    color: Colors.white,
    fontWeight: FontWeight.w700,
  );

  final TextStyle textStyleCommon = TextStyle(
    fontSize: 14,
    color: Colors.black,
    fontWeight: FontWeight.w500,
  );
}
