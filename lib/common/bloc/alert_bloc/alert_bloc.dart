import 'package:flutter_bloc/flutter_bloc.dart';

import 'alert_event.dart';
import 'alert_state.dart';

class AlertBloc extends Bloc<AlertEvent, AlertState> {
  @override
  AlertState get initialState => AlertHiddenState();

  @override
  Stream<AlertState> mapEventToState(AlertEvent event) async* {
    switch (event.runtimeType) {
      case ShowAlertEvent:
        yield* _showAlertState(event);
        break;
      case ConfirmedAlertEvent:
        yield AlertConfirmedState();
        yield AlertHiddenState();
        break;
      case CancelAlertEvent:
        yield AlertHiddenState();
        break;
    }
  }

  Stream<AlertState> _showAlertState(ShowAlertEvent event) async* {
    if (state is AlertShowedState) {
      return;
    }
    yield AlertShowedState(event.message, event.showCancel);
  }
}
