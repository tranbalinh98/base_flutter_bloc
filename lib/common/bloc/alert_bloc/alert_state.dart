abstract class AlertState {}

class AlertShowedState extends AlertState {
  final String message;
  final bool showCancel;
  AlertShowedState(this.message, this.showCancel);
}

class AlertHiddenState extends AlertState {
  AlertHiddenState();
}

class AlertConfirmedState extends AlertState {
  AlertConfirmedState();
}
