import 'package:flutter/material.dart';

abstract class AlertEvent {}

class ShowAlertEvent extends AlertEvent {
  final String message;
  final bool showCancel;

  ShowAlertEvent({@required this.message, this.showCancel = false});
}

class ConfirmedAlertEvent extends AlertEvent {
  ConfirmedAlertEvent();
}

class CancelAlertEvent extends AlertEvent {
  CancelAlertEvent();
}
