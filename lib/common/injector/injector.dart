import 'package:hand_made_flutter_application/common/bloc/alert_bloc/alert_bloc.dart';
import 'package:hand_made_flutter_application/common/bloc/loading_bloc/loading_bloc.dart';
import 'package:hand_made_flutter_application/common/network/network_status.dart';
import 'package:hand_made_flutter_application/common/network/client.dart';
import 'package:hand_made_flutter_application/feature/authencation/presentation/blocs/login_bloc/login_bloc.dart';
import 'package:hand_made_flutter_application/feature/home/presentation/blocs/home_bloc.dart';
import 'package:kiwi/kiwi.dart';

part 'injector.g.dart';

//   flutter packages pub run build_runner build --delete-conflicting-outputs
abstract class Injector {
  static Container container;
  static final resolve = container.resolve;

  static void setup() {
    container = Container();
    _$Injector()._configure();
  }

  void _configure() {
    _configureBlocs();
    // _configureUseCases();
    // _configureRepositories();
    // _configureRemoteDataSources();
//    _configureLocalDataSources();
    _configureCommon();
  }

  @Register.singleton(LoadingBloc)
  @Register.singleton(AlertBloc)
  @Register.factory(LoginBloc)
  @Register.singleton(HomeBloc)
  void _configureBlocs();

  @Register.singleton(Client)
  @Register.factory(NetWorkStatusImpl)
  void _configureCommon();
}
