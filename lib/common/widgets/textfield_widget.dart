import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hand_made_flutter_application/common/constants/numbers.dart';
import 'package:hand_made_flutter_application/common/theme/app_colors.dart';
import 'package:hand_made_flutter_application/common/theme/app_text_theme.dart';
import 'package:hand_made_flutter_application/common/utils/screen_utils.dart';

class TextFieldWidget extends StatefulWidget {
  final String label;
  final String errorText;
  final FocusNode node;
  final TextStyle textStyle;
  final TextEditingController controller;
  final TextInputType inputType;
  final Function onChange;
  final Function onTab;
  final Function onDone;

  TextFieldWidget(
      {@required this.label,
      @required this.errorText,
      this.node,
      this.textStyle,
      this.controller,
      this.inputType,
      this.onChange,
      this.onTab,
      this.onDone});

  @override
  _TextFieldWidgetState createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  TextStyle textStyle;
  TextInputType inputType;

  @override
  void initState() {
    super.initState();
    textStyle = widget.textStyle ?? AppTextTheme().textStyleCommon;
    inputType = widget.inputType ?? TextInputType.text;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(5)),
      child: TextFormField(
        maxLines: 1,
        maxLength: NumberConstants.textInputMaxLength,
        controller: widget.controller,
        style: textStyle,
        onTap: widget.onTab,
        onChanged: widget.onChange,
        onSaved: widget.onDone,
        keyboardType: inputType,
        focusNode: widget.node,
        decoration: InputDecoration(
          border: OutlineInputBorder(
              borderRadius:
                  BorderRadius.all(Radius.circular(30)),
              gapPadding: 0.5),
          contentPadding: EdgeInsets.only(left: 20),
          labelText: widget.label,
          labelStyle: textStyle.copyWith(color: AppColors.primaryColor, fontWeight: FontWeight.normal),
          counter: const SizedBox(),
        ),
      ),
    );
  }
}
