import 'package:flutter/material.dart';
import 'package:hand_made_flutter_application/common/constants/icons.dart';
import 'package:hand_made_flutter_application/common/widgets/svg_icon.dart';

class EmptyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: SvgIcon(
        image: IconConstants.slash,
      ),
    );
  }
}
