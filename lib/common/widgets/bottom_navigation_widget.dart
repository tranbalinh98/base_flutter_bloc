import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hand_made_flutter_application/common/theme/app_colors.dart';
import 'package:hand_made_flutter_application/common/utils/screen_utils.dart';

typedef OnTabChanged = void Function(int index);

class BottomNavigation extends StatefulWidget {
  final List<Widget> tabViews;
  final List<String> icons;
  final Color activeColor;
  final Color inActiveColor;
  final double iconSize;
  final OnTabChanged onTabChanged;
  final int initIndex;

  BottomNavigation({
    Key key,
    @required this.tabViews,
    @required this.icons,
    this.initIndex = 0,
    this.activeColor = AppColors.primaryColor,
    this.inActiveColor = AppColors.warmGrey,
    this.iconSize = 22,
    this.onTabChanged,
  })  : assert(tabViews != null, 'Tab view not be null'),
        assert(icons != null, 'Icons not be null'),
        super(key: key);

  @override
  State<StatefulWidget> createState() => BottomNavigationState();
}

class BottomNavigationState extends State<BottomNavigation> {
  int selectedIndex;

  @override
  void initState() {
    selectedIndex = widget.initIndex;
    super.initState();
  }

  void changeToTabIndex(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final tabs = widget.icons.asMap().entries.map(
      (entry) {
        final int index = entry.key;
        final String source = entry.value;
        final bool isSelected = index == selectedIndex;
        return source != null
            ? Expanded(
                child: Material(
                  color: isSelected ? AppColors.softGreen : Colors.white,
                  child: InkWell(
                    highlightColor: AppColors.softGrey,
                    splashColor: AppColors.softGrey,
                    onTap: () {
                      if (!isSelected) {
                        setState(() {
                          selectedIndex = index;
                        });
                        if (widget.onTabChanged != null) {
                          widget.onTabChanged(selectedIndex);
                        }
                      }
                    },
                    child: Container(
                      height: ScreenUtil().setHeight(60),
                      decoration: BoxDecoration(
                        border: Border(
                          top: BorderSide(
                            color:
                                isSelected ? widget.activeColor : Colors.white,
                            width: 2,
                          ),
                        ),
                      ),
                      child: Center(
                        child: SvgPicture.asset(
                          '$source',
                          width: widget.iconSize,
                          height: widget.iconSize,
                          color: isSelected
                              ? widget.activeColor
                              : widget.inActiveColor,
                        ),
                      ),
                    ),
                  ),
                ),
              )
            : const Spacer();
      },
    ).toList();
    return Scaffold(
      body: IndexedStack(
        index: selectedIndex,
        children: widget.tabViews,
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(children: tabs),
      ),
    );
  }
}
