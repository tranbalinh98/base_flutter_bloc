import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:hand_made_flutter_application/common/bloc/alert_bloc/alert_bloc.dart';
import 'package:hand_made_flutter_application/common/bloc/alert_bloc/alert_event.dart';
import 'package:hand_made_flutter_application/common/constants/strings.dart';
import 'package:hand_made_flutter_application/common/injector/injector.dart';
import 'package:hand_made_flutter_application/feature/routes.dart';

// ignore: must_be_immutable
class AlertWidget<T extends Object> extends StatefulWidget {
  final BuildContext context;
  final String title;
  final bool showCancel;

  AlertWidget({
    Key key,
    this.context,
    this.title,
    this.showCancel,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _AlertState<T>();
  }
}

class _AlertState<K extends Object> extends State<AlertWidget>
    with TickerProviderStateMixin {
  FocusScopeNode _focusScopeNode;
  FocusAttachment _focusAttachment;
  AnimationController _controller;
  Animation<Offset> _offsetAnimation;

  @override
  void initState() {
    super.initState();
    _focusScopeNode = FocusScopeNode();
    _focusAttachment = _focusScopeNode.attach(context);
    _controller = AnimationController(
      duration: const Duration(milliseconds: 100),
      vsync: this,
    )..forward();
    _offsetAnimation = Tween<Offset>(
      begin: const Offset(0, -2.0),
      end: Offset.zero,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeInOut,
    ));
  }

  @override
  void dispose() {
    _focusScopeNode.dispose();
    _focusAttachment.detach();
    _controller.dispose();
    super.dispose();
  }

  Widget _buildTitleText() => Text(
        translate(StringConstants.notification),
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
      );

  Widget _buildContentText() => Padding(
        padding: const EdgeInsets.only(left: 15, right: 15, top: 5, bottom: 25),
        child: Text(
          widget.title ?? '',
          textAlign: TextAlign.center,
        ),
      );

  Widget _buildLine() => Divider(
        height: 1,
        color: Colors.grey[300],
      );

  Widget _buildActionButton(String title, {Function onTap}) => InkWell(
        onTap: onTap,
        child: Center(
          child: Text(
            translate(title),
            style: TextStyle(
                color: Colors.blue, fontWeight: FontWeight.w600, fontSize: 17),
          ),
        ),
      );

  Widget _buildActions() => Container(
        height: 50,
        child: Row(
          children: <Widget>[
            widget.showCancel
                ? Expanded(
                    child:
                        _buildActionButton(StringConstants.cancel, onTap: () {
                    Injector .resolve<AlertBloc>().add(CancelAlertEvent());
                    Routes.instance.pop();
                  }))
                : const SizedBox(),
            Expanded(
                child: _buildActionButton(StringConstants.ok, onTap: () {
              Injector.resolve<AlertBloc>().add(ConfirmedAlertEvent());
              Routes.instance.pop();
            }))
          ],
        ),
      );
  BoxDecoration boxDecoration = BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(10),
      boxShadow: [
        BoxShadow(
          color: Colors.grey[300],
          blurRadius: 5,
          offset: const Offset(0.5, 0.5),
        )
      ]);

  Widget _buildAlert() {
    return Wrap(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 40),
          alignment: Alignment.center,
          decoration: boxDecoration,
          padding: const EdgeInsets.only(top: 20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _buildTitleText(),
              _buildContentText(),
              _buildLine(),
              _buildActions()
            ],
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      child: Material(
        color: Colors.black.withOpacity(0.1),
        child: Center(
          child: SlideTransition(
            position: _offsetAnimation,
            child: _buildAlert(),
          ),
        ),
      ),
    );
  }
}
