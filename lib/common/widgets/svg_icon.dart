import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hand_made_flutter_application/common/theme/app_colors.dart';
import 'package:hand_made_flutter_application/common/utils/screen_utils.dart';

class SvgIcon extends StatelessWidget {
  final String image;
  final double size;
  final Color color;

  const SvgIcon({
    Key key,
    @required this.image,
    this.size = ScreenUtil.iconSize,
    this.color =AppColors.softGreen,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(image,
    height: size,
        width: size,
        color: color,
    );
  }
}
