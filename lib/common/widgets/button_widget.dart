import 'package:flutter/material.dart';
import 'package:hand_made_flutter_application/common/theme/app_colors.dart';
import 'package:hand_made_flutter_application/common/theme/app_text_theme.dart';
import 'package:hand_made_flutter_application/common/utils/screen_utils.dart';

const _defaultHeight = 51.0;

class ButtonWidget extends StatelessWidget {
  final String text;
  final Function onTap;
  final EdgeInsets margin;
  final bool disable;

  const ButtonWidget({
    Key key,
    @required this.text,
    @required this.onTap,
    this.margin,
    this.disable = false,
  })  : assert(text != null, 'Text is not nulll'),
        super(key: key);

  EdgeInsets get _defaultMargin => EdgeInsets.symmetric(
        horizontal: ScreenUtil().setWidth(67),
      );

  @override
  Widget build(BuildContext context) {
    final double height = ScreenUtil().setHeight(_defaultHeight);
    return Opacity(
      opacity: disable ? 0.5 : 1,
      child: Container(
        width: double.infinity,
        height: height,
        margin: margin ?? _defaultMargin,
        child: Material(
          color: AppColors.primaryColor,
          type: MaterialType.button,
          borderRadius: BorderRadius.circular(height / 2),
          child: InkWell(
            onTap: () {
              if (!disable) {
                onTap();
              }
            },
            borderRadius: BorderRadius.circular(height / 2),
            child: Center(
              child: Text(
                text,
                style: AppTextTheme().textStyleButton,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
