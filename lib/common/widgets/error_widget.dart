import 'package:flutter/material.dart';
import 'package:hand_made_flutter_application/common/constants/icons.dart';
import 'package:hand_made_flutter_application/common/theme/app_text_theme.dart';
import 'package:hand_made_flutter_application/common/widgets/svg_icon.dart';

class ErrorWidget extends StatelessWidget {
  final String message;
  final String icon;
  final Function onTab;

  const ErrorWidget({Key key, this.message, this.icon = IconConstants.warning, this.onTab}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: InkWell(
        onTap: onTab,
        child: Column(
          children: [
            SvgIcon(
              image: icon,
              color: Colors.grey,
            ),
            Text(message, style: AppTextTheme().textStyleCommon,),
          ],
        ),
      ),
    );
  }
}
