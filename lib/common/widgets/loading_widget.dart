import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hand_made_flutter_application/common/utils/screen_utils.dart';

class LoadingWidget extends StatelessWidget {
  final bool dismissAble;
  const LoadingWidget({Key key, this.dismissAble = false}): super(key: key);
  @override
  Widget build(BuildContext context) {
    return  WillPopScope(
        onWillPop: () async => Future(() => dismissAble),
    child: Container(
    width: double.infinity,
    height: double.infinity,
    color: Colors.black.withOpacity(0.3),
      child: Center(
        child: SizedBox(
          height: ScreenUtil.iconSize,
          width: ScreenUtil.iconSize,
          child: CupertinoActivityIndicator(),
        ),
      ),
    ),);
  }
}
