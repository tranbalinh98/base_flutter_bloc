const image_path = 'assets/images/';

class ImageConstants{
  static const background_night = '${image_path}background_night.webp';
  static const logo = '${image_path}logo.png';
}