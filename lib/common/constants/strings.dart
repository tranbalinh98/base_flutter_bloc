class StringConstants {
  static const notification = 'label.notification';
  static const cancel = 'button.cancel';
  static const ok = 'button.ok';
  static const connectLost = 'message.connection_lost';
  static const unknowError = 'message.unknow_error';
  static const sloganApp = 'app.slogan_app';
}
class AuthencationStringConstants{
  static const loginError = 'message.login_error';
  static const email = 'label.email';
  static const phoneNumber = 'label.phone_number';
  static const password = 'label.password';
  static const login = 'button.login';
  static const logout = 'label.logout';
  static const forgotPassword = 'label.forgot_password';
  static const rememberMe = 'label.remember_me';
}
