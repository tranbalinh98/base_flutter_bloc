import 'package:intl/intl.dart';

extension DateTimeExtension on DateTime {
  String toFormat(String format) {
    final DateFormat dateFormat = DateFormat(format);
    return dateFormat.format(this) ?? '';
  }
}
