import 'package:dio/dio.dart';

extension DioExtension on DioError {
  String get errorMessage {
    if (type == DioErrorType.RESPONSE) {
      return response.statusMessage;
    }
    return message;
  }
}
