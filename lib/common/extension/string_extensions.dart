import 'package:hand_made_flutter_application/common/constants/regex.dart';

extension StringExtension on String {
  bool get isEmptyOrNull {
    if (this == null) {
      return true;
    }
    return isEmpty;
  }

  bool get isNotEmptyAndNull => this != null && isNotEmpty;

  bool get isValidEmailString =>
      this != null && RegExp(RegexConstants.validEmailRegex).hasMatch(trim());
}
