class RouteName {
  static const String initial = '/';
  static const String login = 'login';
  static const String home = 'home';
  static const String demo = 'demo';
  static const String alert = 'alert';
  static const String loading = 'loading';
  static const String main = 'main';
  static const String splash = 'splash';
}
