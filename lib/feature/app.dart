import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_translate/localized_app.dart';

import 'package:hand_made_flutter_application/common/bloc/alert_bloc/alert_bloc.dart';
import 'package:hand_made_flutter_application/common/bloc/alert_bloc/alert_state.dart';
import 'package:hand_made_flutter_application/common/bloc/loading_bloc/loading_bloc.dart';
import 'package:hand_made_flutter_application/common/bloc/loading_bloc/loading_event.dart';
import 'package:hand_made_flutter_application/common/bloc/loading_bloc/loading_state.dart';
import 'package:hand_made_flutter_application/common/injector/injector.dart';
import 'package:hand_made_flutter_application/common/navigation/route_names.dart';
import 'package:hand_made_flutter_application/common/theme/app_colors.dart';
import 'package:hand_made_flutter_application/common/utils/screen_utils.dart';
import 'package:hand_made_flutter_application/feature/routes.dart';

class App extends StatelessWidget {
  List<BlocProvider> _getProviders() => [
    BlocProvider<LoadingBloc>(
        create: (_) => Injector.resolve<LoadingBloc>()),
    BlocProvider<AlertBloc>(
      create: (_) => Injector.resolve<AlertBloc>(),
    )
  ];

  List<BlocListener> _getBlocListener(context) => [
    BlocListener<AlertBloc, AlertState>(listener: _mapAlertState),
    BlocListener<LoadingBloc, LoadingState>(listener: _mapLoadingState)
  ];

  void _mapAlertState(BuildContext context, AlertState state) {
    if (state is AlertShowedState) {
      Routes.instance.navigateTo(RouteName.alert,
          arguments: {'title': state.message, 'showCancel': state.showCancel});
    }
  }

  void _mapLoadingState(BuildContext context, LoadingState state) {
    if (state is Loading) {
      Routes.instance.navigateTo(RouteName.loading);
//      Future.delayed(const Duration(seconds: 5)).then((_) {
//        Injector.resolve<LoadingBloc>().add(FinishLoading());
//      });
    }
    if (state is Loaded) {
      Routes.instance.pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    final localizationDelegate = LocalizedApp.of(context).delegate;

    return MultiBlocProvider(
      providers: _getProviders(),
      child: MaterialApp(
        navigatorKey: Routes.instance.navigatorKey,
        title: 'Hand made',
        onGenerateRoute: Routes.generateRoute,
        initialRoute: RouteName.splash,
        theme: ThemeData(
          primaryColor: AppColors.primaryColor,
        ),
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          localizationDelegate
        ],
        supportedLocales: localizationDelegate.supportedLocales,
        locale: localizationDelegate.currentLocale,
        builder: (context, widget) {
          ScreenUtil.init(context);
          return MultiBlocListener(
            listeners: _getBlocListener(context),
            child: widget,
          );
        },
      ),
    );
  }
}
