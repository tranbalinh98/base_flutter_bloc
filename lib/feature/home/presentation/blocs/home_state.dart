part of 'home_bloc.dart';

@immutable
abstract class HomeState {}

class HomeInitial extends HomeState {}
class HomeLoadingData extends HomeState{}
class HomeLogOut extends HomeState{}
class HomeLoadedData extends HomeState{
  final int num;
  HomeLoadedData({this.num});
}
class HomeError extends HomeState{
  final String message;
  HomeError({this.message});
}
