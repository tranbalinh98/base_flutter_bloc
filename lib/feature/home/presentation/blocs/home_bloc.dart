import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

part 'home_event.dart';

part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  @override
  HomeState get initialState => HomeInitial();

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    switch (event.runtimeType) {
      case OnLoadData:
        yield* _mapToOnLoadData(event);
        break;
      case OnLogout:
        _mapToOnLogOut(event);
        break;
      default:
        yield* _mapToError(event);
    }
  }

  Stream<HomeState> _mapToOnLoadData(HomeEvent event) async* {
    try {
      yield HomeLoadingData();
      await Future.delayed(Duration(seconds: 3));
      yield HomeLoadedData(num: 20);
    } catch (e) {
      yield HomeError(message: 'Home Error');
    }
  }

  Stream<HomeState> _mapToOnLogOut(HomeEvent event) async* {
    try {
      yield HomeLogOut();
    } catch (e) {
      yield HomeError(message: 'Home Error');
    }
  }

  Stream<HomeState> _mapToError(HomeEvent event) async* {
    yield HomeLoadingData();
    await Future.delayed(Duration(seconds: 3));
    yield HomeError(message: 'Home Load fail');
  }
}
