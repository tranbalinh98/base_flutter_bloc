part of 'home_bloc.dart';

@immutable
abstract class HomeEvent {}

class OnLoadData extends HomeEvent{}
class OnLoadDataFail extends HomeEvent{}
class OnLogout extends HomeEvent{}
