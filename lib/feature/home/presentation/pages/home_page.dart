import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hand_made_flutter_application/common/bloc/alert_bloc/alert_bloc.dart';
import 'package:hand_made_flutter_application/common/bloc/alert_bloc/alert_event.dart';
import 'package:hand_made_flutter_application/common/bloc/loading_bloc/loading_bloc.dart';
import 'package:hand_made_flutter_application/common/bloc/loading_bloc/loading_event.dart';
import 'package:hand_made_flutter_application/common/injector/injector.dart';
import 'package:hand_made_flutter_application/common/navigation/route_names.dart';
import 'package:hand_made_flutter_application/common/widgets/loading_widget.dart';
import 'package:hand_made_flutter_application/feature/authencation/presentation/blocs/login_bloc/login_bloc.dart';
import 'package:hand_made_flutter_application/feature/home/presentation/blocs/home_bloc.dart';

import '../../../routes.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  HomeBloc _homeBloc;

  @override
  void initState() {
    super.initState();
    _homeBloc = Injector.resolve<HomeBloc>()..add(OnLoadData());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener(
        bloc: _homeBloc,
        listener: (context, state) {},
        child: Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.green,
          child: Column(
            children: [
              SizedBox(
                height: 150,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: InkWell(
                        onTap: () => _homeBloc.add(OnLoadData()),
                        child: Container(
                            color: Colors.grey,
                            height: 50,
                            alignment: Alignment.center,
                            child: Text('Loading')),
                      ),
                    ),
                    Expanded(
                      child: InkWell(
                        onTap: () => _homeBloc.add(OnLoadDataFail()),
                        child: Container(
                            color: Colors.grey,
                            height: 50,
                            alignment: Alignment.center,
                            child: Text('Load Fail')),
                      ),
                    ),
                    Expanded(
                      child: InkWell(
                        onTap: () =>
                            Routes.instance.navigateAndRemove(RouteName.login),
                        child: Container(
                            color: Colors.grey,
                            height: 50,
                            alignment: Alignment.center,
                            child: Text('LogOut')),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(190),
                      topRight: Radius.circular(30)),
                  child: Container(
                      color: Colors.white,
                      child: BlocConsumer<HomeBloc, HomeState>(
                        bloc: _homeBloc,
                        listener: (context, state) async {
                          if (state is HomeError) {
                            Injector.resolve<AlertBloc>().add(ShowAlertEvent(
                                message: '${state.message}', showCancel: true));
                          }
                        },
                        builder: (context, state) {
                          if (state is HomeLoadingData) {
                            return LoadingWidget();
                          } else if (state is HomeLoadedData) {
                            return ListView.builder(
                              itemCount: state.num,
                              itemBuilder: (context, index) {
                                return SizedBox(
                                  height: 80,
                                  width: double.infinity,
                                  child: Container(
                                    alignment: Alignment.center,
                                    margin: EdgeInsets.all(10),
                                    color: Colors.grey,
                                    child: Text('$index'),
                                  ),
                                );
                              },
                            );
                          } else {
                            return Center(
                              child: Text('No Data'),
                            );
                          }
                        },
                      )),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
