import 'package:flutter/material.dart';
import 'package:hand_made_flutter_application/common/navigation/fade_in_route.dart';
import 'package:hand_made_flutter_application/common/navigation/route_names.dart';
import 'package:hand_made_flutter_application/common/widgets/alert_widget.dart';
import 'package:hand_made_flutter_application/common/widgets/loading_widget.dart';
import 'package:hand_made_flutter_application/feature/home/presentation/pages/home_page.dart';
import 'package:hand_made_flutter_application/feature/splash/presentation/pages/splash_page.dart';

import 'authencation/presentation/pages/Login_page.dart';

class Routes {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  factory Routes() => _instance;

  Routes._internal();

  static final Routes _instance = Routes._internal();

  static Routes get instance => _instance;

  Future<dynamic> navigateTo(String routeName, {dynamic arguments}) async {
    return navigatorKey.currentState.pushNamed(routeName, arguments: arguments);
  }

  Future<dynamic> navigateAndRemove(String routeName,
      {dynamic arguments}) async {
    return navigatorKey.currentState.pushNamedAndRemoveUntil(
      routeName,
          (Route<dynamic> route) => false,
      arguments: arguments,
    );
  }

  Future<dynamic> navigateAndReplace(String routeName,
      {dynamic arguments}) async {
    return navigatorKey.currentState
        .pushReplacementNamed(routeName, arguments: arguments);
  }

  pop({dynamic result}) {
    return navigatorKey.currentState.pop(result);
  }

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case RouteName.login:
        return FadeInRoute(widget: LoginPage());
      case RouteName.splash:
        return FadeInRoute(widget: SplashPage());
      case RouteName.home:
        return FadeInRoute(widget: HomePage());
      case RouteName.alert:
        final Map argument = settings.arguments;
        return PageRouteBuilder(
          pageBuilder: (_, __, ___) => AlertWidget(
            title: argument['title'] ?? '',
            showCancel: argument['showCancel'] ?? false,
          ),
          opaque: false,
        );
      case RouteName.loading:
        final bool dismissAble = settings.arguments;
        return PageRouteBuilder(
          pageBuilder: (_, __, ___) => LoadingWidget(dismissAble: dismissAble),
          opaque: false,
        );
      default:
        return _emptyRoute(settings);
    }
  }

  static MaterialPageRoute _emptyRoute(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (context) => Scaffold(
        backgroundColor: Colors.green,
        appBar: AppBar(
          leading: InkWell(
            onTap: () => Navigator.of(context).pop(),
            child: const Center(
              child: Text(
                'Back',
                style: TextStyle(fontSize: 16),
              ),
            ),
          ),
        ),
        body: Center(
          child: Text('No path for ${settings.name}'),
        ),
      ),
    );
  }
}
