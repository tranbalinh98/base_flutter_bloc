//import 'dart:io';
//
//import 'package:dio/dio.dart';
//import 'package:young_kids_app/common/network/client.dart';
//
//class PersonalRemoteDataSource {
//  final Client client;
//
//  PersonalRemoteDataSource({this.client});
//  Future<String> updateAvatar(File avatarFile, int id) async {
//    final formData = FormData();
//    formData.files.addAll([
//      MapEntry(
//        'file',
//        MultipartFile.fromFileSync(avatarFile.path),
//      ),
//    ]);
//    final res = await client.post(
//      'v1/api/users/$id/upload-avatar',
//      body: formData,
//    );
//    return res.data['data']['avatar'];
//  }
//}
