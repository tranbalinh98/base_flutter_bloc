//import 'dart:io';
//
//import 'package:flutter/material.dart';
//import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:flutter_svg/flutter_svg.dart';
//import 'package:flutter_translate/flutter_translate.dart';
//import 'package:image_picker/image_picker.dart';
//import 'package:intl/intl.dart';
//import 'package:young_kids_app/common/blocs/app_state/domain/entities/profile_entity.dart';
//import 'package:young_kids_app/common/blocs/app_state/domain/usecases/app_usecase.dart';
//import 'package:young_kids_app/common/constants/icon_constants.dart';
//import 'package:young_kids_app/common/constants/image_constants.dart';
//import 'package:young_kids_app/common/constants/string_constants.dart';
//import 'package:young_kids_app/common/extensions/srceen_extensions.dart';
//import 'package:young_kids_app/common/injector/injector.dart';
//import 'package:young_kids_app/common/navigation/route_names.dart';
//import 'package:young_kids_app/common/themes/app_text_theme.dart';
//import 'package:young_kids_app/common/utils/screen_utils.dart';
//import 'package:young_kids_app/common/widgets/alert_confirm_widget.dart';
//import 'package:young_kids_app/features/personal/presentation/bloc/personal_bloc.dart';
//import 'package:young_kids_app/common/widgets/custom_avatar.dart';
//import 'package:young_kids_app/features/personal/presentation/widgets/option.dart';
//import 'package:young_kids_app/features/routes.dart';
//
//class PersonalPage extends StatefulWidget {
//  @override
//  _PersonalPageState createState() => _PersonalPageState();
//}
//
//class _PersonalPageState extends State<PersonalPage> {
//  final ImagePicker picker = ImagePicker();
//  PersonalBloc personalBloc;
//  @override
//  void initState() {
//    personalBloc = Injector.resolve<PersonalBloc>();
//    super.initState();
//  }
//
//  @override
//  void dispose() {
//    super.dispose();
//  }
//
//  String _getMemberSince(int createDate) {
//    final date = DateTime.fromMillisecondsSinceEpoch(createDate);
//    return DateFormat('MMMM yyyy').format(date);
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    final _radius = 30.w;
//    return Scaffold(
//      body: BlocConsumer<PersonalBloc, PersonalState>(
//        bloc: personalBloc,
//        listener: (context, state) {
//          if (state is LogOutLoaded) {
//            Routes.instance.navigateAndRemove(RouteName.login);
//          }
//        },
//        builder: (conetxt, state) {
//          ProfileEntity _profile;
//          String name = '', avatar = '';
//          _profile = Injector.resolve<AppUseCase>().profile;
//          name = _profile.manager != null
//              ? _profile.manager.name
//              : _profile.players[0].name;
//          if (_profile.avatar == null) {
//            avatar = '';
//          } else {
//            avatar = _profile.avatar;
//          }
//          if (state is UpdatedAvatar) {
//            avatar = state.avatar;
//          }
//          return Stack(
//            children: <Widget>[
//              Container(
//                width: ScreenUtil.screenWidth,
//                child: Image.asset(
//                  ImageConstants.background,
//                  fit: BoxFit.cover,
//                ),
//              ),
//              Container(
//                height: 125.h,
//                alignment: Alignment.topRight,
//                padding: EdgeInsets.only(
//                  top: 25.h + ScreenUtil.statusBarHeight,
//                  right: 24.5.w,
//                ),
//                child: InkWell(
//                  onTap: () {
//                    Alert.show(
//                      context,
//                      message: translate(StringConstants.conFirmLogOut),
//                      showCancel: true,
//                      onConfirmed: () => personalBloc.add(OnLogOut()),
//                    );
//                  },
//                  child: SvgPicture.asset(
//                    IconConstants.logOut,
//                    width: 23.5.w,
//                    height: 23.h,
//                    color: Colors.white,
//                  ),
//                ),
//              ),
//              Container(
//                margin:
//                    EdgeInsets.only(top: 125.h + ScreenUtil.statusBarHeight),
//                decoration: BoxDecoration(
//                  color: Colors.white,
//                  borderRadius: BorderRadius.vertical(
//                    top: Radius.circular(_radius),
//                  ),
//                ),
//              ),
//              Positioned(
//                top: 45.h + ScreenUtil.statusBarHeight,
//                bottom: 0,
//                left: 0,
//                right: 0,
//                child: Column(
//                  children: <Widget>[
//                    CustomAvatar(
//                      fullName: name,
//                      avatar: avatar,
//                      updateAvatar: () async {
//                        final pickedFile = await picker.getImage(
//                          source: ImageSource.gallery,
//                          maxHeight: 300.0,
//                          maxWidth: 300.0,
//                        );
//                        if (pickedFile != null) {
//                          personalBloc.add(
//                            OnUpdateAvatar(
//                              File(pickedFile.path),
//                              _profile.id,
//                            ),
//                          );
//                        }
//                      },
//                    ),
//                    Padding(
//                      padding: EdgeInsets.only(top: 20.h),
//                      child: Text(
//                        name,
//                        style: AppTextTheme()
//                            .textStyleAppbar
//                            .copyWith(color: Colors.black),
//                      ),
//                    ),
//                    Padding(
//                      padding: EdgeInsets.only(top: 5.w, bottom: 19.h),
//                      child: Text(
//                        // ignore: lines_longer_than_80_chars
//                        '${translate(StringConstants.memberSince)}: ${_profile == null ? '' : _getMemberSince(_profile.createDate)}',
//                        style: AppTextTheme()
//                            .textStyleInput
//                            .copyWith(fontSize: 17.5.sp),
//                      ),
//                    ),
//                    Expanded(
//                      child: Option(),
//                    ),
//                  ],
//                ),
//              ),
//            ],
//          );
//        },
//      ),
//    );
//  }
//}
