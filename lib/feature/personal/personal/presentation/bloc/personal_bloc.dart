//import 'dart:async';
//import 'dart:io';
//
//import 'package:bloc/bloc.dart';
//import 'package:young_kids_app/common/blocs/alert_bloc/alert_bloc.dart';
//import 'package:young_kids_app/common/blocs/alert_bloc/alert_event.dart';
//import 'package:young_kids_app/common/blocs/app_state/domain/entities/profile_entity.dart';
//import 'package:young_kids_app/common/blocs/app_state/domain/usecases/app_usecase.dart';
//import 'package:young_kids_app/common/blocs/loading_bloc/loading_bloc.dart';
//import 'package:young_kids_app/common/blocs/loading_bloc/loading_event.dart';
//import 'package:young_kids_app/features/personal/domain/usecases/personal_usecase.dart';
//
//part 'personal_event.dart';
//part 'personal_state.dart';
//
//class PersonalBloc extends Bloc<PersonalEvent, PersonalState> {
//  final AppUseCase appUseCase;
//  final AlertBloc alertBloc;
//  final LoadingBloc loadingBloc;
//  final PersonalUseCase personalUseCase;
//
//  PersonalBloc({
//    this.loadingBloc,
//    this.personalUseCase,
//    this.appUseCase,
//    this.alertBloc,
//  });
//
//  @override
//  PersonalState get initialState => PersonalInitial();
//  @override
//  Stream<PersonalState> mapEventToState(
//    PersonalEvent event,
//  ) async* {
//    switch (event.runtimeType) {
//      case OnLogOut:
//        yield* _onLogOut();
//        break;
//      case OnUpdateAvatar:
//        yield* _onUpdateAvatar(event);
//        break;
//      default:
//    }
//  }
//
//  Stream<PersonalState> _onLogOut() async* {
//    try {
//      yield LogOutLoading();
//      await appUseCase.deleteToken();
//      yield LogOutLoaded();
//    } catch (e) {
//      alertBloc.add(ShowAlertEvent(message: e.toString()));
//      yield LogOutLoaded();
//    }
//  }
//
//  Stream<PersonalState> _onUpdateAvatar(OnUpdateAvatar event) async* {
//    try {
//      yield UpdatingAvatar();
//      loadingBloc.add(StartLoading());
//      final avatar =
//          await personalUseCase.updateAvatar(event.avatarFile, event.id);
//      appUseCase.profile.avatar = avatar;
//      loadingBloc.add(FinishLoading());
//      yield UpdatedAvatar(avatar: avatar);
//    } catch (e) {
//      loadingBloc.add(FinishLoading());
//      alertBloc.add(ShowAlertEvent(message: e.toString()));
//      yield UpdateAvatarError();
//    }
//  }
//}
