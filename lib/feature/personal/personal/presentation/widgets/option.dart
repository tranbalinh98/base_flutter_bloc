//import 'package:flutter/material.dart';
//import 'package:flutter_translate/flutter_translate.dart';
//import 'package:young_kids_app/common/constants/icon_constants.dart';
//import 'package:young_kids_app/common/constants/string_constants.dart';
//import 'package:young_kids_app/common/navigation/route_names.dart';
//import 'package:young_kids_app/common/themes/app_text_theme.dart';
//import 'package:young_kids_app/features/personal/presentation/widgets/item_option.dart';
//import 'package:young_kids_app/common/extensions/srceen_extensions.dart';
//import 'package:young_kids_app/features/routes.dart';
//
//class Option extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return Column(
//      children: <Widget>[
//        ItemOption(
//          iconSource: IconConstants.update,
//          label: translate(
//            StringConstants.update,
//          ),
//          isSelected: true,
//        ),
//        ItemOption(
//          iconSource: IconConstants.people,
//          label: translate(
//            StringConstants.management,
//          ),
//          isSelected: false,
//          onTap: () {
//            Routes.instance.navigateTo(RouteName.playerManagement);
//          },
//        ),
//        ItemOption(
//          iconSource: IconConstants.history,
//          label: translate(
//            StringConstants.history,
//          ),
//          isSelected: false,
//        ),
//        ItemOption(
//          iconSource: IconConstants.lock,
//          label: translate(
//            StringConstants.changePassword,
//          ),
//          isSelected: false,
//          onTap: () {
//            Routes.instance.navigateTo(RouteName.changePassword);
//          },
//        ),
//        ItemOption(
//          iconSource: IconConstants.star,
//          label: translate(
//            StringConstants.rate,
//          ),
//          isSelected: false,
//        ),
//        ItemOption(
//          iconSource: IconConstants.shareFriends,
//          label: translate(
//            StringConstants.share,
//          ),
//          isSelected: false,
//        ),
//        ItemOption(
//          iconSource: IconConstants.about,
//          label: translate(
//            StringConstants.about,
//          ),
//          isSelected: false,
//        ),
//        Padding(
//          padding: EdgeInsets.only(top: 22.5.h),
//          child: Text(
//            'Version: 1.0 (12)',
//            style: AppTextTheme().textStyleInput,
//          ),
//        )
//      ],
//    );
//  }
//}
