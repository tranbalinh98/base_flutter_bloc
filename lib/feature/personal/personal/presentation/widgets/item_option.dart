//import 'package:flutter/material.dart';
//import 'package:flutter_svg/flutter_svg.dart';
//import 'package:young_kids_app/common/constants/icon_constants.dart';
//import 'package:young_kids_app/common/extensions/srceen_extensions.dart';
//import 'package:young_kids_app/common/themes/app_colors.dart';
//import 'package:young_kids_app/common/themes/app_text_theme.dart';
//import 'package:young_kids_app/common/widgets/vertical_line_widget.dart';
//
//class ItemOption extends StatelessWidget {
//  final String iconSource;
//  final String label;
//  final bool isSelected;
//  final Function onTap;
//
//  const ItemOption(
//      {Key key, this.iconSource, this.label, this.isSelected, this.onTap})
//      : super(key: key);
//
//  @override
//  Widget build(BuildContext context) {
//    final _color = isSelected ? AppColors.primaryColor : AppColors.warmGrey;
//    return Container(
//      height: 59.5.h,
//      padding: EdgeInsets.symmetric(horizontal: 24.5.w),
//      child: Column(
//        mainAxisAlignment: MainAxisAlignment.end,
//        children: <Widget>[
//          Row(
//            children: <Widget>[
//              SizedBox(
//                width: 22.w,
//                child: SvgPicture.asset(
//                  iconSource,
//                  width: 22.w,
//                  height: 20.w,
//                  color: _color,
//                ),
//              ),
//              Expanded(
//                child: Padding(
//                  padding: EdgeInsets.only(left: 22.w),
//                  child: Text(
//                    label,
//                    style: AppTextTheme().textStyleLabel.copyWith(
//                        color:
//                            isSelected ? AppColors.primaryColor : Colors.black,
//                        letterSpacing: 0.6.w,
//                        fontWeight: FontWeight.w700),
//                  ),
//                ),
//              ),
//              InkWell(
//                onTap: onTap,
//                child: Padding(
//                  padding: EdgeInsets.only(left: 30.w, top: 15.w, bottom: 15.w),
//                  child: SvgPicture.asset(
//                    IconConstants.next,
//                    width: 7.w,
//                    height: 12.5.w,
//                    color: _color,
//                  ),
//                ),
//              ),
//            ],
//          ),
//          SizedBox(height: 7.5.h),
//          VerticalLine(thinkness: 0.5.h),
//        ],
//      ),
//    );
//  }
//}
