import 'dart:io';

abstract class PersonalRepository {
  Future<String> updateAvatar(File avatarFile, int id);
}
