import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:hand_made_flutter_application/common/bloc/loading_bloc/loading_state.dart';
import 'package:hand_made_flutter_application/common/constants/strings.dart';
import 'package:hand_made_flutter_application/common/enum/app_enum.dart';
import 'package:hand_made_flutter_application/common/utils/log_utils.dart';
import 'package:meta/meta.dart';

part 'login_event.dart';

part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  @override
  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    switch (event.runtimeType) {
      case OnLoginTempUser:
        yield* _loginWithTempUser(event);
        break;
        case OnLoginTempUser:
        yield* _loginWithTempUserFail(event);
        break;
      default:
        yield* _loginFail(event);
    }
  }

  Stream<LoginState> _loginWithTempUser(OnLoginTempUser event) async* {
    try {
      yield Logging();
      await Future.delayed(Duration(seconds: 3));
      yield LoginSuccess(type: UserType.Temp, id: '009900');
    } catch (e) {
      LOG.e(' $e');
      yield LoginError(
          message: translate(AuthencationStringConstants.loginError));
    }
  }  Stream<LoginState> _loginWithTempUserFail (OnLoginTempUserFail event) async* {
    try {
      yield Logging();
      await Future.delayed(Duration(seconds: 3));
      yield LoginError(
          message: translate(AuthencationStringConstants.loginError));
    } catch (e) {
      LOG.e(' $e');
      yield LoginError(
          message: translate(AuthencationStringConstants.loginError));
    }
  }

  Stream<LoginState> _loginFail(LoginEvent event) async* {
    yield Logging();
    await Future.delayed(Duration(seconds: 3));
    yield LoginError(
        message: translate(AuthencationStringConstants.loginError));
  }
}
