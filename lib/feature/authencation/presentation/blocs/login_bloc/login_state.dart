part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}
class Logging extends LoginState {}

class LoginSuccess extends LoginState{
  final String id;
  final UserType type;
  LoginSuccess({this.id, this.type});
}

class LoginError extends LoginState{
  final String message;
  LoginError({this.message});
}