part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class OnLoginWithFacebook extends LoginEvent{
}

class OnLoginWithEmail extends LoginEvent{
  final String email;
  final String password;
  OnLoginWithEmail({this.email, this.password});
}

class OnLoginWithPhoneNumber extends LoginEvent{
  final String phone;
  final String nationalCode;
  OnLoginWithPhoneNumber({this.phone, this.nationalCode});
}

class OnRememberUser extends LoginEvent{
  final isRemember;
  OnRememberUser({this.isRemember});
}

class OnForgotPassword extends LoginEvent{}

class OnLoginTempUser extends LoginEvent{}
class OnLoginTempUserFail extends LoginEvent{}