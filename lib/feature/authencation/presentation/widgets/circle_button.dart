import 'package:flutter/material.dart';
import 'package:hand_made_flutter_application/common/utils/screen_utils.dart';

class CircleButton extends StatelessWidget {
  final double size;
  final Function onTap;
  final String icon;
  final Color borderColor;

  CircleButton({
    @required this.size,
    @required this.onTap,
    @required this.icon,
    this.borderColor = Colors.green,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(size/2)),
        child: Container(
          height: size,
            width: size,
          padding: EdgeInsets.all(2),
          color: Colors.white,
        ),
      ),
    );
  }
}
