import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:hand_made_flutter_application/common/bloc/alert_bloc/alert_bloc.dart';
import 'package:hand_made_flutter_application/common/bloc/alert_bloc/alert_event.dart';
import 'package:hand_made_flutter_application/common/bloc/alert_bloc/alert_state.dart';
import 'package:hand_made_flutter_application/common/bloc/loading_bloc/loading_bloc.dart';
import 'package:hand_made_flutter_application/common/bloc/loading_bloc/loading_event.dart';
import 'package:hand_made_flutter_application/common/bloc/loading_bloc/loading_state.dart';
import 'package:hand_made_flutter_application/common/constants/strings.dart';
import 'package:hand_made_flutter_application/common/injector/injector.dart';
import 'package:hand_made_flutter_application/common/navigation/route_names.dart';
import 'package:hand_made_flutter_application/common/theme/app_colors.dart';
import 'package:hand_made_flutter_application/common/theme/app_text_theme.dart';
import 'package:hand_made_flutter_application/common/utils/log_utils.dart';
import 'package:hand_made_flutter_application/common/utils/screen_utils.dart';
import 'package:hand_made_flutter_application/common/widgets/button_widget.dart';
import 'package:hand_made_flutter_application/common/widgets/textfield_widget.dart';
import 'package:hand_made_flutter_application/feature/authencation/presentation/blocs/login_bloc/login_bloc.dart';
import 'package:hand_made_flutter_application/feature/routes.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  LoginBloc _loginBloc;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _loginBloc = LoginBloc();
    return Scaffold(
      body: BlocConsumer<LoginBloc, LoginState>(
        bloc: _loginBloc,
        listener: (context, state) {
          if (state is LoginError) {
            Injector.resolve<LoadingBloc>().add(FinishLoading());
            Injector.resolve<AlertBloc>()
                .add(ShowAlertEvent(message: '${state.message}', showCancel: true));
          }
          if (state is Logging) {
            Injector.resolve<LoadingBloc>().add(StartLoading());
          }
          if (state is LoginSuccess) {
            Routes.instance.navigateAndRemove(RouteName.home);
          }
        },
        builder: (context, state) {
          return SafeArea(
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: ScreenUtil.paddingHorizontal,
              ),
              decoration: BoxDecoration(
                color: AppColors.softGreen,
              ),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          left: ScreenUtil.blocWith,
                          right: ScreenUtil.blocWith,
                          top: ScreenUtil.blocHeight),
                      child: Text(
                        translate(AuthencationStringConstants.login),
                        style: AppTextTheme()
                            .textStyleAppbar
                            .copyWith(fontSize: 30),
                      ),
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(80),
                    ),
                    TextFieldWidget(
                      label: translate(AuthencationStringConstants.email),
                      errorText: translate(StringConstants.unknowError),
                      inputType: TextInputType.emailAddress,
                    ),
                    TextFieldWidget(
                      label: translate(AuthencationStringConstants.password),
                      errorText: translate(StringConstants.unknowError),
                      inputType: TextInputType.visiblePassword,
                    ),
                    InkWell(
                      onTap: null,
                      child: Row(
                        children: [
                          Checkbox(
                            value: true,
                          ),
                          Text(
                            translate(AuthencationStringConstants.rememberMe),
                            style: AppTextTheme().textStyleCommon,
                          ),
                          const Spacer(),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ButtonWidget(
                      text: 'LOGIN SUCCESS',
                      onTap: () => _loginBloc.add(OnLoginTempUser()),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ButtonWidget(
                      text: 'LOGIN FAIL',
                      onTap: () => _loginBloc.add(OnLoginTempUserFail()),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
