import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:hand_made_flutter_application/common/constants/images.dart';
import 'package:hand_made_flutter_application/common/constants/strings.dart';
import 'package:hand_made_flutter_application/common/navigation/route_names.dart';
import 'package:hand_made_flutter_application/common/theme/app_colors.dart';
import 'package:hand_made_flutter_application/common/theme/app_text_theme.dart';
import 'package:hand_made_flutter_application/common/utils/screen_utils.dart';
import 'package:hand_made_flutter_application/feature/routes.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration(seconds: 3)).whenComplete(() =>
        Routes.instance.navigateAndRemove(RouteName.login)
    );
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          alignment: Alignment.center,
          padding: EdgeInsets.all(ScreenUtil.paddingHorizontal),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Padding(
                padding: EdgeInsets.only(
                    left: ScreenUtil.blocWith,
                    right: ScreenUtil.blocWith,
                    top: ScreenUtil.blocHeight),
                child: Image.asset(
                  '${ImageConstants.logo}',
                  fit: BoxFit.contain,
                ),
              ),
              Text(
                translate(
                  StringConstants.sloganApp,
                ),
                maxLines: 2,
                style:
                    AppTextTheme().textStyleLabel.copyWith(color: Colors.green),
              ),
              const Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}
